package ru.edu;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.model.Athlete;
import ru.edu.screens.Screen;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class ApplicationTest {
    @Mock
    private InputStream in;

    @Mock
    private PrintStream out;

    @Mock
    private Screen mainScreen;

    @Mock
    private Competition competition;

    @Mock
    private Scanner scanner;

    Application app;

    @Before
    public void setup() {
        openMocks(this);
        app = new Application(in, out, competition, mainScreen, scanner);
    }

    @Test(timeout = 1000)
    public void run(){
        when(mainScreen.readInput(competition, scanner)).thenReturn(mainScreen, null);
        when(competition.register(any(Athlete.class))).thenThrow(new IllegalArgumentException("mock exception"));

        app.run();

        verify(mainScreen, times(2)).promt();
        verify(mainScreen, times(2)).readInput(competition, scanner);
    }


}
