package ru.edu.screens;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.Competition;
import ru.edu.CompetitionImpl;
import ru.edu.model.AthleteImpl;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class RegisterScreenTest {
    @Mock
    private InputStream in;

    @Mock
    private PrintStream out;

    @Mock
    private Scanner scanner;

    @Mock
    private RegisterScreen screen;
    private Competition competition;

    private static final String FIRST_NAME = "Oliver";
    private static final String LAST_NAME = "Smith";
    private static final String COUNTRY_NAME = "UK";

    @Before
    public void setup() {
        openMocks(this);
        screen = new RegisterScreen(in, out);
        competition = new CompetitionImpl();
        competition.register(new AthleteImpl(FIRST_NAME, LAST_NAME, COUNTRY_NAME));
    }

    @Test(timeout = 1000)
    public void testPromt() {
        screen.promt();

    }

    @Test(timeout = 1000)
    public void testReadInput() {
        Screen tmp;

        when(scanner.nextLine()).thenReturn("name");
        tmp = screen.readInput(competition, scanner);
        assertEquals(screen, tmp);

        when(scanner.nextLine()).thenReturn("lastname");
        tmp = screen.readInput(competition, scanner);
        assertEquals(screen, tmp);

        when(scanner.nextLine()).thenReturn("country");
        tmp = screen.readInput(competition, scanner);
        assertEquals(null, tmp);
    }

    @Test(expected = IllegalStateException.class)
    public void testReadInputEqualAthlete() {
        when(scanner.nextLine()).thenReturn(FIRST_NAME);
        screen.readInput(competition, scanner);

        when(scanner.nextLine()).thenReturn(LAST_NAME);
        screen.readInput(competition, scanner);

        when(scanner.nextLine()).thenReturn(COUNTRY_NAME);
        screen.readInput(competition, scanner);
    }

}