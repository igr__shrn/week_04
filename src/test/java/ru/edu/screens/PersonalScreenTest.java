package ru.edu.screens;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.Competition;
import ru.edu.CompetitionImpl;
import ru.edu.model.AthleteImpl;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class PersonalScreenTest {
    @Mock
    private InputStream in;

    @Mock
    private PrintStream out;

    @Mock
    private Scanner scanner;

    private PersonalScreen screen;

    @Mock
    private Competition competition;

    private static final String FIRST_NAME = "Oliver";
    private static final String LAST_NAME = "Smith";
    private static final String COUNTRY_NAME = "UK";

    @Before
    public void setup() {
        openMocks(this);
        screen = new PersonalScreen(in, out);
        competition = new CompetitionImpl();
        competition.register(new AthleteImpl(FIRST_NAME, LAST_NAME, COUNTRY_NAME));
    }

    @Test(timeout = 1000)
    public void testPromt() {
        screen.promt();
    }

    @Test(timeout = 1000)
    public void testReadInput0() {
        when(scanner.nextInt()).thenReturn(0);
        assertEquals(screen, screen.readInput(competition, scanner));
    }

    @Test(timeout = 1000)
    public void testReadInput1() {
        when(scanner.nextInt()).thenReturn(1);
        assertEquals(null, screen.readInput(competition, scanner));
    }
}