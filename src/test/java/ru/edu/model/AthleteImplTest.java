package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AthleteImplTest {

    public static final String FIRST_NAME = "Oliver";
    public static final String LAST_NAME = "Smith";
    public static final String COUNTRY_NAME = "UK";

    public static final String FIRST_NAME2 = "John";
    public static final String LAST_NAME2 = "Doe";
    public static final String COUNTRY_NAME2 = "USA";

    private AthleteImpl athlete;
    private AthleteImpl athlete2;

    @Before
    public void setUp() {
        athlete = new AthleteImpl(FIRST_NAME, LAST_NAME, COUNTRY_NAME);
        athlete2 = new AthleteImpl(FIRST_NAME2, LAST_NAME2, COUNTRY_NAME2);
    }

    @Test
    public void testFirstName() {
        assertEquals(FIRST_NAME, athlete.getFirstName());
    }

    @Test
    public void testLastName() {
        assertEquals(LAST_NAME, athlete.getLastName());
    }

    @Test
    public void testCountry() {
        assertEquals(COUNTRY_NAME, athlete.getCountry());
    }

    @Test
    public void testEquals() {
        assertTrue(athlete.equals(new AthleteImpl(FIRST_NAME, LAST_NAME, COUNTRY_NAME)));
    }

    @Test
    public void testEqualsNull() {
        assertFalse(athlete.equals(new Object()));
    }

    @Test
    public void testOtherLastname() {
        assertFalse(athlete.equals(new AthleteImpl(FIRST_NAME, LAST_NAME2, COUNTRY_NAME)));
    }

    @Test
    public void testOtherCountryName() {
        assertFalse(athlete.equals(new AthleteImpl(FIRST_NAME, LAST_NAME, COUNTRY_NAME2)));
    }

    @Test
    public void testToString() {
        assertEquals("Имя='Oliver', Фамилия='Smith', Страна='UK'", athlete.toString());
    }
}