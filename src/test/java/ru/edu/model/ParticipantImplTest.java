package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ParticipantImplTest {

    public static final String FIRST_NAME = "Oliver";
    public static final String LAST_NAME = "Smith";
    public static final String COUNTRY_NAME = "UK";

    public static final String FIRST_NAME2 = "John";
    public static final String LAST_NAME2 = "Doe";
    public static final String COUNTRY_NAME2 = "USA";

    private AthleteImpl athlete;

    private ParticipantImpl participant;
    private ParticipantImpl participant2;

    @Before
    public void setUp() {
        athlete = new AthleteImpl(FIRST_NAME, LAST_NAME, COUNTRY_NAME);

        participant = new ParticipantImpl(athlete, 1L);
        participant2 = new ParticipantImpl(athlete, 2L);
    }

    @Test
    public void testId() {
        assertEquals(1L, (long) participant.getId());
    }

    @Test
    public void testScore() {
        assertEquals(0, participant.getScore());
    }

    @Test
    public void testAddScore() {
        participant.updateScore(10);
        assertEquals(10, participant.getScore());

        participant.updateScore(-5L);
        assertEquals(5, participant.getScore());
    }

    @Test
    public void testGetAthlete() {
        assertEquals(athlete.hashCode(), participant.getAthlete().hashCode());
    }

    @Test
    public void testEquals() {
        assertTrue(participant.equals(participant));
    }

    @Test
    public void testEqualsOtherParticipant() {
        assertFalse(participant.equals(participant2));
    }

    @Test
    public void testEqualsOtherAthlete() {
        participant2 = new ParticipantImpl(new AthleteImpl(FIRST_NAME2, LAST_NAME2, COUNTRY_NAME2), 1L);
        assertFalse(participant.equals(participant2));
    }

    @Test
    public void testEqualsObject() {
        assertFalse(participant.equals(new Object()));
    }

    @Test
    public void testEqualsNull() {
        assertFalse(participant.equals(null));
    }

    @Test
    public void testEqualsOtherAthleteId() {
        participant2 = new ParticipantImpl(athlete, 2L);
        assertFalse(participant.equals(participant2));
    }

    @Test
    public void testHashCode() {
        participant2 = new ParticipantImpl(athlete, 1L);
        assertTrue(participant.hashCode() == participant2.hashCode());
    }

    @Test
    public void testToString() {
        assertEquals("\n" +
                "Участник #1. Очки - 0 {Имя='Oliver', Фамилия='Smith', Страна='UK'}", participant.toString());
    }

}