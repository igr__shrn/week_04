package ru.edu.model;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CountryParticipantImplTest {

    public static final String FIRST_NAME = "Oliver";
    public static final String LAST_NAME = "Smith";
    public static final String COUNTRY_NAME = "UK";

    public static final String FIRST_NAME2 = "John";
    public static final String LAST_NAME2 = "Doe";
    public static final String COUNTRY_NAME2 = "USA";

    private AthleteImpl athlete;
    private AthleteImpl athlete1;
    private AthleteImpl athlete2;

    private ParticipantImpl participant;
    private ParticipantImpl participant1;
    private ParticipantImpl participant2;

    CountryParticipantImpl country;
    List<Participant> participantsList;

    @Before
    public void setUp() {
        athlete = new AthleteImpl(FIRST_NAME, LAST_NAME, COUNTRY_NAME);
        athlete1 = new AthleteImpl(FIRST_NAME2, LAST_NAME2, COUNTRY_NAME);
        athlete2 = new AthleteImpl(FIRST_NAME2, LAST_NAME2, COUNTRY_NAME2);

        participant = new ParticipantImpl(athlete, 1L);
        participant1 = new ParticipantImpl(athlete1, 2L);
        participant2 = new ParticipantImpl(athlete, 3L);

        country = new CountryParticipantImpl(COUNTRY_NAME);

        participantsList = new ArrayList<>();
        participantsList.add(participant);
        participantsList.add(participant1);
    }

    @Test
    public void testGetName() {
        assertEquals(COUNTRY_NAME, country.getName());
    }

    @Test
    public void testGetParticipants() {
        country.addParticipant(participant);
        country.addParticipant(participant1);
        assertEquals(participantsList, country.getParticipants());
    }

    @Test
    public void testAddParticipant() {
        country.addParticipant(new ParticipantImpl(athlete, 2L));
    }

    @Test
    public void testGetScore() {
        assertEquals(0, country.getScore());
    }

    @Test
    public void testSetScore() {
        country.setScore(15);
        assertEquals(15, country.getScore());
    }

}