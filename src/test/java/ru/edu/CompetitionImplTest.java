package ru.edu;

import org.junit.Before;
import org.junit.Test;
import ru.edu.model.Athlete;
import ru.edu.model.AthleteImpl;
import ru.edu.model.Participant;
import ru.edu.model.ParticipantImpl;

import static org.junit.Assert.*;

public class CompetitionImplTest {

    public static final String FIRST_NAME = "Oliver";
    public static final String LAST_NAME = "Smith";
    public static final String COUNTRY_NAME = "UK";

    public static final String FIRST_NAME2 = "John";
    public static final String LAST_NAME2 = "Doe";
    public static final String COUNTRY_NAME2 = "USA";

    public static final String COUNTRY_NAME3 = "DE";

    private Athlete athlete;
    private Athlete athlete2;

    private ParticipantImpl participant;
    private ParticipantImpl participant2;

    private CompetitionImpl competition;

    @Before
    public void setUp() {
        athlete = new AthleteImpl(FIRST_NAME, LAST_NAME, COUNTRY_NAME);
        athlete2 = new AthleteImpl(FIRST_NAME2, LAST_NAME2, COUNTRY_NAME2);

        competition = new CompetitionImpl();
        competition.register(athlete);

        participant = new ParticipantImpl(athlete, 1L);
        participant2 = new ParticipantImpl(athlete2, 2L);
    }

    @Test
    public void testRegister() {
        assertEquals(participant2, competition.register(athlete2));
    }

    @Test(expected = IllegalStateException.class)
    public void testRegisterIllegalArgument() {
        competition.register(athlete);
    }

    @Test
    public void testUpdateScoreById() {
        competition.updateScore(1, 10);
        assertEquals(10, competition.getParticipantMap().get(1L).getScore());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateScoreByNegativeId() {
        competition.updateScore(-1, 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateScoreByIndexOutOfArray() {
        competition.updateScore(10, 10);
    }

    @Test
    public void testUpdateScoreByParticipant() {
        competition.updateScore(participant, 10);
        assertEquals(10, competition.getParticipantMap().get(1L).getScore());
    }

    /**
     * 20 участников
     * Максимальное количество очков у участника №10 - 27 очков. В цикле при i = 9
     * Второе место должно быть у участника №24 - 24 очка. В цикле при i = 8
     */
    @Test
    public void testGetResults() {
        CompetitionImpl competition = new CompetitionImpl();
        for (int i = 0; i < 20; i++) {

            if (i < 5) {
                athlete = new AthleteImpl(FIRST_NAME + i, LAST_NAME + i, COUNTRY_NAME);
                Participant tmpParticipant = competition.register(athlete);
                competition.updateScore(tmpParticipant.getId(), i);
            } else if (i < 10) {
                athlete = new AthleteImpl(FIRST_NAME2 + i, LAST_NAME2 + i, COUNTRY_NAME2);
                Participant tmpParticipant = competition.register(athlete);
                competition.updateScore(tmpParticipant.getId(), i * 3);
            } else {
                athlete = new AthleteImpl(FIRST_NAME + i, LAST_NAME + i, COUNTRY_NAME3);
                Participant tmpParticipant = competition.register(athlete);
                competition.updateScore(tmpParticipant.getId(), i);
            }
        }
        System.out.println(competition.getResults());
    }

    /**
     * 20 участников
     * <p>
     * #1 место DE, 10 участников - 145 в сумме.
     * #2 место USA, 5 участников - 105 в сумме.
     * #3 место UK, 5 участников - 10 в сумме.
     */
    @Test
    public void testGetParticipantsCountries() {
        CompetitionImpl competition = new CompetitionImpl();
        for (int i = 0; i < 20; i++) {
            if (i < 5) {
                athlete = new AthleteImpl(FIRST_NAME + i, LAST_NAME + i, COUNTRY_NAME);
                Participant tmpParticipant = competition.register(athlete);
                competition.updateScore(tmpParticipant.getId(), i);
            } else if (i < 10) {
                athlete = new AthleteImpl(FIRST_NAME2 + i, LAST_NAME2 + i, COUNTRY_NAME2);
                Participant tmpParticipant = competition.register(athlete);
                competition.updateScore(tmpParticipant.getId(), i * 3);
            } else {
                athlete = new AthleteImpl(FIRST_NAME2 + i, LAST_NAME2 + i, COUNTRY_NAME3);
                Participant tmpParticipant = competition.register(athlete);
                competition.updateScore(tmpParticipant.getId(), i);
            }
        }
        System.out.println(competition.getParticipantsCountries());
    }

}