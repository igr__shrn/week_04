package ru.edu;

import ru.edu.screens.MainScreen;

import java.util.Scanner;

public class ConsoleEntryPoint {
    public static void main(String[] args) {

        Application app = new Application(
                System.in,
                System.out,
                new CompetitionImpl(),
                new MainScreen(System.in, System.out),
                new Scanner(System.in)
        );

        app.run();
    }
}
