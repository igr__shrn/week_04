package ru.edu.screens;

import ru.edu.Competition;
import ru.edu.model.AthleteImpl;
import ru.edu.model.Participant;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class RegisterScreen implements Screen {

    private int step = 0;

    private String name;
    private String lastName;
    private String country;

    private Participant participant;

    private String[] promptsForSteps = {
            "Введите имя: ", //step = 0
            "Введите фамилию: ",
            "Введите страну: "

    };

    private InputStream input;
    private PrintStream output;
    private AthleteImpl athleteForRegistation = null;

    public RegisterScreen(InputStream in, PrintStream out) {
        input = in;
        output = out;
    }

    /**
     * Выводит на экран и подсказывает какой ввод ожидается.
     */
    @Override
    public void promt() {
        if (step < 3) {
            output.println(promptsForSteps[step]);
        }
    }

    /**
     * Считаем пользовательский ввод, если требуется.
     * Выполним действия.
     * Вернем this если остаемся на этом экране, null если возвращаемся назад.
     *
     * @param competition
     * @return
     */
    @Override
    public Screen readInput(Competition competition, Scanner scanner) {
        String answer = scanner.nextLine();

        if (step == 0) {
            name = answer;
        }
        if (step == 1) {
            lastName = answer;
        }
        if (step == 2) {
            country = answer;
            step++;
            participant = registerAthlete(competition);
            output.println("\nЗарегистрирован: " + participant.toString() + "\n");
            return null;
        }
        step++;
        return this;
    }

    private Participant registerAthlete(Competition competition) {
        athleteForRegistation = new AthleteImpl(name, lastName, country);
        output.println(athleteForRegistation.hashCode());
        try {
            Participant participant = competition.register(athleteForRegistation);
            return participant;
        } catch (IllegalStateException e) {
            throw new IllegalStateException(e);
        }
    }

}
