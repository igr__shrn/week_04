package ru.edu.screens;

import ru.edu.Competition;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class PersonalScreen implements Screen {

    private InputStream input;
    private PrintStream output;

    public PersonalScreen(InputStream in, PrintStream out) {
        input = in;
        output = out;
    }

    /**
     * Выводит на экран и подсказывает какой ввод ожидается.
     */
    @Override
    public void promt() {
        output.println("-------");
        output.println("Таблица участников");
    }

    /**
     * Считаем пользовательский ввод, если требуется.
     * Выполним действия.
     * Вернем this если остаемся на этом экране, null если возвращаемся назад.
     *
     * @param competition
     * @return
     */
    @Override
    public Screen readInput(Competition competition, Scanner scanner) {
        output.println(competition.getResults());
        output.println("-------");
        output.println("Выберите действие:");
        output.println("1) Вернуться на начальный экран");

        int answer = scanner.nextInt();

        switch (answer) {
            case 1:
                return null;
            default:
                return this;
        }

    }
}
