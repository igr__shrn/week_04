package ru.edu.screens;

import ru.edu.Competition;

import java.util.Scanner;

public interface Screen {

    /**
     * Выводит на экран и подсказывает какой ввод ожидается.
     */
    void promt();

    /**
     * Считаем пользовательский ввод, если требуется.
     * Выполним действия.
     * Вернем this если остаемся на этом экране, null если возвращаемся назад.
     *
     * @param competition
     * @param scanner
     * @return
     */
    Screen readInput(Competition competition, Scanner scanner);
}
