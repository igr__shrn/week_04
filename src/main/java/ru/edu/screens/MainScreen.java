package ru.edu.screens;

import ru.edu.Competition;
import ru.edu.model.AthleteImpl;
import ru.edu.model.Participant;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Random;
import java.util.Scanner;

public class MainScreen implements Screen {

    private InputStream input;
    private PrintStream output;

    private String[] country = {
            "UK",
            "RU",
            "USA",
            "DE",
            "ESP"
    };

    private String FIRST_NAME = "Oliver";
    private String LAST_NAME = "Smith";

    public MainScreen(InputStream in, PrintStream out) {
        input = in;
        output = out;
    }

    /**
     * Выводит на экран и подсказывает какой ввод ожидается.
     */
    @Override
    public void promt() {
        output.println("Выберите действие:");
        output.println("1) Регистрация участника");
        output.println("2) Симуляция регистрации участников");
        output.println("3) Личный зачет");
        output.println("4) Командный зачет");
        output.println("0) Выход");
    }

    /**
     * Считаем пользовательский ввод, если требуется.
     * Выполним действия.
     * Вернем this если остаемся на этом экране, null если возвращаемся назад.
     *
     * @param competition
     * @return
     */
    @Override
    public Screen readInput(Competition competition, Scanner scanner) {
        int answer = scanner.nextInt();
        switch (answer) {
            case 1:
                return new RegisterScreen(input, output);
            case 2:
                simulation(competition);
                return this;
            case 3:
                return new PersonalScreen(input, output);
            case 4:
                return new CountryScreen(input, output);
            case 0:
                return null;
            default:
                return this;
        }
    }

    /**
     * Симуляция регистрации участников.
     *
     * @param competition
     */
    private void simulation(Competition competition) {
        Random random = new Random();

        for (int i = 0; i < 20; i++) {
            int randomNumber = random.nextInt(5);

            Participant tmpParticipant = competition.register(
                    new AthleteImpl(FIRST_NAME + i, LAST_NAME + i, country[randomNumber]));

            competition.updateScore(tmpParticipant.getId(), i * randomNumber);
        }
    }
}
