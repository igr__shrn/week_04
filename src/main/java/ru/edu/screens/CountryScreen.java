package ru.edu.screens;

import ru.edu.Competition;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class CountryScreen implements Screen {

    private InputStream input;
    private PrintStream output;

    public CountryScreen(InputStream in, PrintStream out) {
        input = in;
        output = out;
    }

    /**
     * Выводит на экран и подсказывает какой ввод ожидается.
     */
    @Override
    public void promt() {
        output.println("-------");
        output.println("Таблица стран участников");
    }

    /**
     * Считаем пользовательский ввод, если требуется.
     * Выполним действия.
     * Вернем this если остаемся на этом экране, null если возвращаемся назад.
     *
     * @param competition
     * @return
     */
    @Override
    public Screen readInput(Competition competition, Scanner scanner) {
        output.println(competition.getParticipantsCountries());
        output.println("-------");
        output.println("Выберите действие:");
        output.println("1) Вернуться на начальный экран");

        int answer = scanner.nextInt();

        switch (answer) {
            case 1:
                return null;
            default:
                return this;
        }
    }
}
