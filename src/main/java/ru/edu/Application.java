package ru.edu;

import ru.edu.screens.Screen;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.Stack;

public class Application {
    private final InputStream in;
    private final PrintStream out;
    private final Competition competition;
    private Stack<Screen> screens;
    private Scanner scanner;

    public Application(InputStream in,
                       PrintStream out,
                       Competition competition,
                       Screen mainScreen,
                       Scanner scan) {
        this.in = in;
        this.out = out;
        this.competition = competition;
        this.scanner = scan;
        screens = new Stack<>();
        screens.push(mainScreen);
    }

    public void run() {
        while (screens.size() > 0) {
            runScreen();
        }

    }

    void runScreen() {
        Screen screen = screens.peek();
        screen.promt();
        Screen newScreen = screen.readInput(competition, scanner);
        if (newScreen != null) {
            if (newScreen != screen) {
                screens.push(newScreen);
            }

        } else {
            screens.pop();
        }

    }
}
