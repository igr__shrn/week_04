package ru.edu;

import ru.edu.model.*;

import java.util.*;

public class CompetitionImpl implements Competition {

    /**
     * Коллекция участников.
     */
    private Map<Long, ParticipantImpl> participantMap = new HashMap<>();

    private Set<Athlete> registeredAthletes = new HashSet<>();

    private Set<String> registeredCountry = new HashSet<>();
    /**
     *
     */
    private Long id = 1L;

    public Map<Long, ParticipantImpl> getParticipantMap() {
        return participantMap;
    }

    /**
     * Регистрация участника.
     *
     * @param participant - участник
     * @return зарегистрированный участник
     * @throws IllegalArgumentException - при попытке повторной регистрации
     */
    @Override
    public Participant register(Athlete participant) {
        if (registeredAthletes.contains(participant)) {
            throw new IllegalStateException("Спортсмен уже зарегистрирован.");
        }
        if (!registeredCountry.contains(participant.getCountry())) {
            registeredCountry.add(participant.getCountry());
        }
        registeredAthletes.add(participant);

        ParticipantImpl registeredParticipant = new ParticipantImpl(participant, id);
        participantMap.put(registeredParticipant.getId(), registeredParticipant);
        id++;

        return registeredParticipant;
    }

    /**
     * Обновление счета участника по его id.
     * Требуется константное время выполнения
     * <p>
     * updateScore(10) прибавляет 10 очков
     * updateScore(-5) отнимает 5 очков
     *
     * @param id    регистрационный номер участника
     * @param score +/- величина изменения счета
     */
    @Override
    public void updateScore(long id, long score) {
        if (id < 0L) {
            throw new IllegalArgumentException("ID участники не может быть меньше 0");
        }
        ParticipantImpl participant = participantMap.get(id);
        if (participant == null) {
            throw new IllegalArgumentException("Участник не найден");
        }
        participant.updateScore(score);
    }

    /**
     * Обновление счета участника по его объекту Participant
     * Требуется константное время выполнения
     *
     * @param participant зарегистрированный участник
     * @param score       новое значение счета
     */
    @Override
    public void updateScore(Participant participant, long score) {
        updateScore(participant.getId(), score);
    }

    /**
     * Получение результатов.
     * Сортировка участников от большего счета к меньшему.
     *
     * @return отсортированный список участников
     */
    @Override
    public List<Participant> getResults() {
        LinkedList<Participant> myParticipants = new LinkedList<>(participantMap.values());
        myParticipants.sort((o1, o2) -> (int) (o2.getScore() - o1.getScore()));

        return myParticipants;
    }

    /**
     * Получение результатов по странам.
     * Группировка участников из одной страны и сумма их счетов.
     * Сортировка результатов от большего счета к меньшему.
     *
     * @return отсортированный список стран-участников
     */
    @Override
    public List<CountryParticipant> getParticipantsCountries() {
        List<CountryParticipant> listResult = new ArrayList<>();
        Map<String, CountryParticipantImpl> listCountry = getStringCountryParticipantMap();

        for (Map.Entry<String, CountryParticipantImpl> entry : listCountry.entrySet()) {
            entry.getValue().
                    getParticipants().
                    sort((o1, o2) -> (int) (o2.getScore() - o1.getScore()));
            listResult.add(entry.getValue());
        }

        listResult.sort((o1, o2) -> (int) (o2.getScore() - o1.getScore()));

        return listResult;
    }

    private Map<String, CountryParticipantImpl> getStringCountryParticipantMap() {
        Map<String, CountryParticipantImpl> listCountry = buildCountryList();

        for (Map.Entry<Long, ParticipantImpl> entry : participantMap.entrySet()) {
            String tmpCountry = entry.getValue().getAthlete().getCountry();

            CountryParticipantImpl country = listCountry.get(tmpCountry);
            country.addParticipant(entry.getValue());
            country.updateScore(entry.getValue().getScore());
        }
        return listCountry;
    }

    private Map<String, CountryParticipantImpl> buildCountryList() {
        Map<String, CountryParticipantImpl> list = new HashMap<>();

        for (String countryName : registeredCountry) {
            CountryParticipantImpl country = new CountryParticipantImpl(countryName);
            list.put(countryName, country);
        }

        return list;
    }

    @Override
    public String toString() {
        return "CompetitionImpl{" +
                "participantMap=" + participantMap +
                '}';
    }
}
