package ru.edu.model;

public class AthleteImpl implements Athlete {
    /**
     * Имя.
     *
     */
    private String firstName;

    /**
     * Фамилия.
     *
     */
    private String lastName;

    /**
     * Страна.
     *
     */
    private String country;

    /**
     *
     * @param firstName
     * @param lastName
     * @param country
     */
    public AthleteImpl(String firstName, String lastName, String country) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
    }

    /**
     * Имя.
     *
     * @return значение
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /**
     * Фамилия.
     *
     * @return значение
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /**
     * Страна.
     *
     * @return значение
     */
    @Override
    public String getCountry() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AthleteImpl other = (AthleteImpl) o;
        if (firstName != other.firstName)
            return false;
        if (lastName != other.lastName)
            return false;
        if (country != other.country)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final long prime = 31;
        long result = 1L;

        result = prime * result + firstName.length();
        result = prime * result + lastName.length();
        result = prime * result + country.length();

        return (int) result;
    }

    @Override
    public String toString() {
        return "Имя='" + firstName + '\'' +
                ", Фамилия='" + lastName + '\'' +
                ", Страна='" + country + '\'';
    }
}
