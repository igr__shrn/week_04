package ru.edu.model;

import java.util.Objects;

public class ParticipantImpl implements Participant {

    /**
     * Атлет.
     */
    private Athlete athlete;

    /**
     * Регистрационный номер атлета.
     */
    private Long id;

    /**
     * Счет участника.
     */
    private long score;

    /**
     *
     * @param athleteInfo
     * @param athleteId
     */
    public ParticipantImpl(Athlete athleteInfo, Long athleteId) {
        athlete = athleteInfo;
        id = athleteId;
    }

    /**
     * Получение информации о регистрационном номере.
     *
     * @return регистрационный номер
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * Информация о спортсмене
     *
     * @return объект спортсмена
     */
    @Override
    public Athlete getAthlete() {
        return athlete;
    }

    /**
     * Обновление счета участника.
     *
     * @param points - количество очков.
     */
    public void updateScore(long points) {
        score += points;
    }

    /**
     * Счет участника.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ParticipantImpl other = (ParticipantImpl) o;
        if (id != other.id)
            return false;
        if (athlete != other.athlete)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final long prime = 31;
        long result = 1L;

        result = prime * result + id;
        result = prime * result + athlete.hashCode();
        return (int) result;
    }

    @Override
    public String toString() {
        return "\nУчастник #" + id + ". Очки - " + score +
                " {" + athlete + '}';
    }
}
