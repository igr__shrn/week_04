package ru.edu.model;

import java.util.ArrayList;
import java.util.List;

public class CountryParticipantImpl implements CountryParticipant {

    /**
     * Название страны.
     */
    private String name;

    /**
     * Список участников страны.
     */
    private List<Participant> participants = new ArrayList<>();

    /**
     * Счет страны.
     */
    private long score;

    /**
     * @param nameCountry
     */
    public CountryParticipantImpl(String nameCountry) {
        name = nameCountry;
    }

    /**
     * Название страны.
     *
     * @return название
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Список участников от страны.
     *
     * @return список участников
     */
    @Override
    public List<Participant> getParticipants() {
        return participants;
    }

    /**
     * Добавление участника соревнования.
     *
     * @param participant
     */
    public void addParticipant(Participant participant) {
        participants.add(participant);
    }

    /**
     * Установка счета стране.
     *
     * @param points
     */
    public void setScore(long points) {
        score = points;
    }

    /**
     * Установка счета стране.
     *
     * @param points
     */
    public void updateScore(long points) {
        score += points;
    }

    /**
     * Счет страны.
     *
     * @return счет
     */
    @Override
    public long getScore() {
        return score;
    }

    @Override
    public String toString() {
        return "\n Страна \"" + name + "\"" +
                "\n Очки - " + score +
                "\n участники: \n" + participants +
                '\n';
    }
}
